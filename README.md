# Teste da Vital Job

## Objetivo

CRUD Básico - Cadastro completo de campos de uma tabela de banco de dados (Listagem, Exibição, Inserção, Atualização e Remoção)

Deverá ser desenvolvido um CRUD contendo pelo menos um campo de cada um dos seguintes tipo de dados: Data, texto curto, texto longo, valor inteiro, valor ponto flutuante, seleção de valores (um somente), seleção de valores (múltiplos).

O artefato deverá ser desenvolvido nas seguintes tecnologias:

* Python 2.7
* Django 1.4
* Postgres
* HTTP Request para envio de informações para front-end
* JSON para retorno de dados.

Serão dados pontos extras caso a interface do usuário seja desenvolvida nas seguintes tecnologias:

* GWT
* Smartgwt


## Criando o banco no PostgreSql

Considere que você tenha a última versão do postgres instalado.

```
sudo su - postgres
createdb vitaljob
createuser -P vitaljob
# pass: vitaljob
exit
```


## Instalação

* Clone o repositório.
* Crie um virtualenv com Python 2.7
* Ative o virtualenv.
* Instale as dependências.
* Configure a instância com o .env
* Rode a migração
* Cadastre as redes sociais
* Cadastre um contato usando o Selenium


```
git clone https://rg3915@bitbucket.org/rg3915/vitaljob-challenge.git
cd vitaljob-challenge
virtualenv -p python2.7 .venv
source .venv/bin/activate
PS1="(`basename \"$VIRTUAL_ENV\"`):/\W$ " # opcional
pip install -r requirements.txt
cp contrib/env-sample .env
python manage.py syncdb
make shell_social
make selenium_person
```
