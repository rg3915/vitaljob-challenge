from myproject.core.models import Social

SOCIAL_NETWORK = [
    'facebook',
    'twitter',
    'google-plus',
    'github',
    'pinterest',
    'linkedin',
    'instagram',
    'skype',
    'slack'
]

obj = [Social(network=val) for val in SOCIAL_NETWORK]
Social.objects.bulk_create(obj)
