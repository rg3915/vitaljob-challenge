# -*- coding: utf-8 -*-
import time
import csv
from random import randint
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from gen_random_values import gen_date, gen_string, gen_number, gen_decimal

page = webdriver.Firefox()
page.maximize_window()
time.sleep(0.5)
page.get('http://localhost:8000/person/add/')

person_list = []

''' Read person.csv '''
with open('fix/person.csv', 'r') as f:
    r = csv.DictReader(f)
    for dct in r:
        person_list.append(dct)
    f.close()

INDEX = randint(0, 9)

fields = [
    ['id_first_name', person_list[INDEX]['first_name']],
    ['id_last_name', person_list[INDEX]['last_name']],
    ['id_email', person_list[INDEX]['email']],
    ['id_birthday', gen_date()],
    ['id_description', gen_string(30) + ' ' +
     gen_string(27) + ' ' + gen_string(20)],
    ['id_child', gen_number()],
    ['id_salary', gen_decimal()],
    ['id_address', person_list[INDEX][u'address']],
    ['id_complement', person_list[INDEX][u'complement']],
    ['id_district', person_list[INDEX][u'district']],
    ['id_city', person_list[INDEX][u'city']],
    ['id_uf', u'São Paulo'],
    ['id_cep', person_list[INDEX]['cep']],
]

for field in fields:
    search = page.find_element_by_id(field[0])
    search.send_keys(field[1])

items = randint(1, 9)
items_list = []

for _ in range(items):
    items_list.append(randint(1, 9))

for i in items_list:
    item = page.find_element_by_xpath(
        '//*[@id="id_network"]/option[' + str(i) + ']')
    # press Ctrl Key
    item.send_keys(Keys.CONTROL)
    # click item
    item.click()


button = page.find_element_by_class_name('btn-primary')
button.click()

page.quit()
