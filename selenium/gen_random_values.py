import string
from random import randint, choice
from datetime import date


def gen_string(max_length):
    return str(''.join(choice(string.ascii_letters) for i in range(max_length)))
gen_string.required = ['max_length']


def gen_number(min_number=0, max_number=10):
    # gera numeros inteiros entre 15 e 99
    return randint(min_number, max_number)


def gen_decimal():
    a = randint(1, 9999)
    b = randint(0, 99)
    return '%s.%s' % (a, b)


def gen_date(min_year=1915, max_year=2016):
    # gera um date no formato yyyy-mm-dd
    year = randint(min_year, max_year)
    month = randint(1, 12)
    day = randint(1, 28)
    return date(year, month, day).isoformat()
