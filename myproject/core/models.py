# -*- coding: utf-8 -*-
from django.db import models
from localflavor.br.br_states import STATE_CHOICES


class TimeStampedModel(models.Model):
    created = models.DateTimeField(
        'criado em', auto_now_add=True, auto_now=False)
    modified = models.DateTimeField(
        'modificado em', auto_now_add=False, auto_now=True)

    class Meta:
        abstract = True


class Address(models.Model):
    address = models.CharField(u'endereço', max_length=100, blank=True)
    complement = models.CharField('complemento', max_length=100, blank=True)
    district = models.CharField('bairro', max_length=100, blank=True)
    city = models.CharField('cidade', max_length=100, blank=True)
    uf = models.CharField('UF', max_length=2,
                          choices=STATE_CHOICES, blank=True)
    cep = models.CharField('CEP', max_length=9, blank=True)

    class Meta:
        abstract = True


class Person(TimeStampedModel, Address):
    first_name = models.CharField('nome', max_length=50)
    last_name = models.CharField(
        'sobrenome', max_length=50, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    birthday = models.DateField('nascimento', null=True, blank=True)
    description = models.TextField(u'descrição', null=True, blank=True)
    child = models.PositiveIntegerField('filhos', null=True, blank=True)
    salary = models.DecimalField(u'salário', max_digits=6, decimal_places=2)
    network = models.ManyToManyField(
        'Social', verbose_name='rede social', null=True, blank=True)

    class Meta:
        ordering = ['first_name']
        verbose_name = 'pessoa'
        verbose_name_plural = 'pessoas'

    def __unicode__(self):
        return ' '.join(filter(None, [self.first_name, self.last_name]))

    full_name = property(__unicode__)

    @models.permalink
    def get_absolute_url(self):
        return ('core:person_detail', (), {'pk': self.pk})


class Social(models.Model):
    network = models.CharField('rede social', max_length=50, unique=True)

    class Meta:
        ordering = ['network']
        verbose_name = 'rede social'
        verbose_name_plural = 'redes sociais'

    def __unicode__(self):
        return self.network
