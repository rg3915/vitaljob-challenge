from django.contrib import admin
from .models import Person, Social
from .forms import PersonForm


class PersonAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'email')
    search_fields = ('first_name', 'last_name',)
    form = PersonForm

admin.site.register(Person, PersonAdmin)
admin.site.register(Social)
