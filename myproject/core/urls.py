from django.conf.urls import patterns, url
from myproject.core.views import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    'myproject.core.views',
    url(r'^$', PersonList.as_view(), name='person_list'),
    url(r'^add/$', 'person_create', name='person_add'),
    url(r'^(?P<pk>\d+)/$', 'person_detail', name='person_detail'),
    url(r'^(?P<pk>\d+)/json/$', 'person_detail_json', name='person_detail_json'),
    url(r'^(?P<pk>\d+)/edit/$', 'person_update', name='person_edit'),
    url(r'^(?P<pk>\d+)/delete/$', 'person_delete', name='person_delete')
)
